"use strict";

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

/*AJAX (Asynchronous Javascript and XML) - технологія, яка дозволяє робити запити на сервер і отримувати відповіді від нього у фоновому режимі (без перезавантаження сторінки). Відсутність перезавантаження позитивно впливає на швидкість роботи веб-сторінок та додатків. AJAX дозволяє робити максимально спеціалізовані запити на конкретне місце документу, і після отримання відповіді сервера змінюється лише дана частина документу, а не вся сторінка, що призводить до економії трафіка та зменшення навантаження на сервер.  */

function showMainInfo() {
  fetchHandler("https://ajax.test-danit.com/api/swapi/films").then(
    (episodes) => {
      episodes.forEach(
        ({
          episodeId: episode,
          name,
          openingCrawl: description,
          characters,
        }) => {
          const episodeList = createAndShowElement("ul", "h1");
          const episodeObj = { episode, description, name };
          const itemToShow = Object.entries(episodeObj)
            .map((episodeObjKeys) => `<li>${episodeObjKeys}</li>`)
            .join("")
            .replaceAll(",", " - ");
          episodeList.insertAdjacentHTML("beforeend", itemToShow);
          showCharactersInfo(characters);
        }
      );
    }
  );
}

function showCharactersInfo(charactersArray) {
  const characterTitle = createAndShowElement("h2", "ul");
  characterTitle.textContent = "List of characters";
  const characterList = createAndShowElement("ol", "h2");
  let requests = charactersArray.map((url) => fetchHandler(url));
  Promise.all(requests).then((charactersArray) =>
    charactersArray.forEach((character) => {
      characterList.insertAdjacentHTML(
        "beforeend",
        `<li>${character.name}</li>`
      );
    })
  );
}

function fetchHandler(url) {
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Error");
    }
  });
}

function createAndShowElement(tag, selector) {
  const element = document.createElement(tag);
  document.querySelector(selector).after(element);
  return element;
}

showMainInfo();
