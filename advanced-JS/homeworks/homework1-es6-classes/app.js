"use strict";

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

/* Прототипне наслідування дозволяє об'єктам розділяти спільні властивості. Наприклад, ми маємо 2 об'єкти: А і В. Об'єкт В наслідується від об'єкту А. Об'єкт А містить метод getName(). Незважаючи на те, що в об'єкта В немає методу getName(), ми зможемо викликати даний метод в об'єкті В завдяки прототипному наслідування. JS спочатку перевірить методи об'єкта В, не знайде серед них getName(), тому піде далі, до прототипу А, знайде цей метод там і виконає його, базуючись на даних об'єкта В. Таким самим чином і об'єкту А, і об'єкту В будуть доступні загальні методи об'єкту (hasOwnProperty(), keys() і т.д.), оскільки їхнім прототипом є Object. */

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

/* Для того щоб викликати батьківську функцію-констуктор і перенести характеристики батьківського класу до успадкованого. Лише після цього ми зможемо доповнювати успадкований клас новими характеристиками. Якщо ми не викличемо super() у конструкторі класу-нащадка, отримаємо помилку "Must call super constructor in derived class before accessing 'this' or returning from derived constructor". */

class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name (){
        return this._name
    }
    set name (newName){
        this._name = newName
    }
    get age (){
        return this._age
    }
    set age (newAge){
       this._age = newAge
    }
    get salary (){
        return this._salary + "$"
    }
    set salary (newSalary){
       this._salary = newSalary
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }
    get salary (){
        return this._salary * 3 + "$"
    }
}

const nickProgrammer = new Programmer("Nick", 24, 500, "JS")
const krisProgrammer = new Programmer("Kris", 27, 650, "Java")
const johnProgrammer = new Programmer("John", 28, 750, "Python")

console.log(nickProgrammer);
console.log(krisProgrammer);
console.log(johnProgrammer);