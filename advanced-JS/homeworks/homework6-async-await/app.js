"use strict";

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
/*Асинхронність у Javascript дозволяє виконувати декілька операцій одночасно. Оскільки JS - однопотоковий, це означає що він може виконувати лише одну операцію в один момент часу. А що якщо дана операція займає тривалий період часу для виконання? За логікою код повинен зупинитися та чекати на завершення операції, а потім перейти до виконання наступних стрічок коду. Все це призвело б до значної затримки виконання програм. У такі моменти на допомогу приходить асинхронність. Завдяки асинхронності операції, які потребують часу для виконання, виконуються у фоновому режимі, подальший код не блокується, а коли асинхронні операції виконалися, вони потрапляються у стек. Асинхронні операції, як правило пов'язані із запитами на сервер та очікуванням відповіді від нього, а також SetTimeout/SetInterval.*/

document.addEventListener("DOMContentLoaded", findByIp);

function findByIp() {
  const btn = document.querySelector(".btn");
  btn.addEventListener("click", getInfoByIp);
}

async function getInfoByIp() {
  const ip = await fetchHandler(`https://api.ipify.org/?format=json`);
  const ipInfo = await fetchHandler(
    `http://ip-api.com/json/${ip.ip}?fields=continent,country,regionName,city,district`
  );
  let { continent, country, regionName: region, city, district } = ipInfo;
  if (!district) {
    district = "Unknown";
  }
  const newObject = { continent, country, region, city, district };
  const itemsToShow = Object.entries(newObject)
    .map((newObjectKeys) => `<li>${newObjectKeys}</li>`)
    .join("")
    .replaceAll(",", " - ");
  const list = document.createElement("ul");
  document.querySelector(".container").insertAdjacentElement("beforeend", list);
  list.innerHTML = itemsToShow;
  const prevSibling = list.previousElementSibling;
  if (!prevSibling.closest(".btn")) {
    prevSibling.remove();
  }
}

async function fetchHandler(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}
