"use strict";

class Card {
  constructor() {
    this.containter = document.createElement("div");
    this.containter.classList.add("container");
  }
  render(post, user) {
    this.containter.innerHTML = `
    <div class="btn_container">
      <span class="close_btn">&times;</span>
    </div>
    <div class="tweet_container">
      <h3 class="title">
      "${post.title}"
      </h3>
      <p class="content">
      ${post.body}
      </p>
      <div class="author_info">
        <a href="mailto:${user.email}">${user.email}</a>
        <p>${user.name}</p>
      </div>
    </div>`;
    document.body.prepend(this.containter);
    this.delete(post.id);
  }
  delete(postId) {
    const closeBtn = this.containter.querySelector(".close_btn");
    closeBtn.addEventListener("click", () => {
      request(
        `https://ajax.test-danit.com/api/json/posts/${postId}`,
        "DELETE"
      ).then(() => this.containter.remove());
    });
  }
}

async function request(url, requestType = "GET") {
  const response = await fetch(url, { method: requestType });
  if (requestType === "DELETE") {
    return;
  } else {
    const data = await response.json();
    return data;
  }
}

async function showPosts() {
  const posts = await request("https://ajax.test-danit.com/api/json/posts");
  const usersData = await Promise.all(
    posts.map((post) =>
      request(`https://ajax.test-danit.com/api/json/users/${post.userId}`)
    )
  );
  for (let i = 0; i < posts.length; i++) {
    const card = new Card();
    card.render(posts[i], usersData[i]);
  }
}

document.addEventListener("DOMContentLoaded", showPosts);
