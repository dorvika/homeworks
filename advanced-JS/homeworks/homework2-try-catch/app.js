"use strict";

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

/*По-перше, конструкцію try...catch доречно використовувати разом із методом JSON.parse(). Тут ми точно не знаємо чи спрацює JSON.parse() коректно, бо отримуємо дані ззовні. І у випадку, якщо метод видасть помилку наш скріпт НЕ зупинить свою роботу повністю, а перенесе своє виконання до блоку catch, в якому ми можемо зробити ще один запит на сервет або показати користувачеві модальне вікно із помилкою, перенести його на іншу сторінку і т.д.
Також try...catch підходить для опрацювання помилок конкретного типу. Наприклад, ми точно знаємо як опрацьовувати певний вид помилки, ми маємо підготовлений код для цього. У блоці try ми починаємо виконувати наш скріпт, зустрічаємо помиоку і кидаємо її (throw) далі. Блок catch її ловить, і якщо це та помилка, яку ми вміємо опрацьовувати, ми її опрацьовуємо; якщо ні - кидаємо далі. Де її зловить або інша конструкція try...catch, або загалом скріпт.
Взагалі дана конструкція дозволяє перестрахуватися та прописати декілька варіантів виконання скріпта, в залежноті від наявності чи відсутності помилки. В основному використовується якщо дані, які нам потрібно опрацювати, приходять ззовні, і програміст не може гарантувати їхню правильність */

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

books.forEach((book) => {
  try {
    const requiredProps = ["author", "price", "name"];
    validateBook(book, requiredProps);
    showBook(book);
  } catch (error) {
    console.log(error.message);
  }
});

function validateBook(bookObj, requiredPropsArray) {
  if (!requiredPropsArray.every((prop) => prop in bookObj)) {
    const missedProp = requiredPropsArray.find((item) => {
      return !Object.keys(bookObj).includes(item);
    });
    throw new Error(`Missed property - "${missedProp}"`);
  }
}

function showBook(bookObj) {
  const list = document.createElement("ul");
  document.getElementById("root").append(list);
  const itemToShow = Object.entries(bookObj)
    .map((bookObjKeys) => `<li>${bookObjKeys}</li>`)
    .join("")
    .replaceAll(",", " - ");
  list.insertAdjacentHTML("beforeend", itemToShow);
}
