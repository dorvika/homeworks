"use strict"

/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу render() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */

class Modal {
    constructor(id, text, classList) {
        this.id = id
        this.text = text
        this.classList = classList
    }
    
    open() {
        this.element.classList.add("active")
    }

    close() {
        this.element.classList.remove("active")
    }

    render() {
        this.element = document.createElement("div");
        this.element.setAttribute("id", this.id)
        this.element.classList.add("modal");
        this.element.classList.add(this.classList);
        this.element.innerHTML= `
            <div class="modal-content">
                <span class="close">X</span>
                <h1>${this.text}</h1>
            </div>`
        document.body.append(this.element)
    }
}

const loginModal = new Modal("login-modal", "Ви успішно увійшли", "login-modal");
loginModal.render();

const signUpModal = new Modal("sign-up-modal", "Реєстрація", "sign-up-modal");
signUpModal.render()

const loginBtn = document.querySelector("#login-btn");
loginBtn.addEventListener("click", () => {
    loginModal.open()
})

const signUpBtn = document.querySelector("#sign-up-btn");
signUpBtn.addEventListener("click", () => {
    signUpModal.open()
})

const closeBtns = document.querySelectorAll(".close");
closeBtns.forEach((btn) => {
    btn.addEventListener("click", () => {
        loginModal.close();
        signUpModal.close()
    })
})