// // /**
// //  * Наявний уже функціонал для одного складу (kyivWarehouse)
// //  * Але з розвитком компанії з'явилося ще 2 склади (lvivWarehouse та kharkivWarehouse)
// //  * відповідно з місткістю 50 та 40 та поки що без продуктів
// //  * 
// //  * Відповідно потрібно зберегти логіку, але додати можливість також
// //  * масштабувати її
// //  * Для цього необхідно переробити об'єкт в функцію-конструктор з методами
// //  * та створити відповідно 3 склади
// //  * 
 
// //  * - додати на склад Львову 7 телефонів, 10 планшетів
// //  * - додати на склад Харькова 10 телефонів та 6 ноутбуків
// //  * - вивести загальну кількість товарів по кожному складу в консоль
// //  * - вивести всі товари кожного складу в консоль
// //  * - дізнатися, якого товару найбільше у Львові
// //  * 
// //  ADVANCED:
// //  - створити окремо функцію getTotalQuantity, якій можна передати назву товару
// //  та отримати загальну кількість товарів по всіх складах
// //  - додати можливість переміщення товарів між складами
// //  для цього додати метод moveProduct(name, quantity, targetWarehouse)
// //  - перемістити 4 ноутбуки з Києва у Львів
// //  * 
// //  */


function Warehouse (maxCapacity, products,){
    this.maxCapacity = maxCapacity;
    this.products = products;

}

Warehouse.prototype.getTotalQuantity = function(){
    let sum = 0;
        for (const product in this.products) {
            sum += this.products[product];
        }
        return sum;
}
Warehouse.prototype.getProductQuantity = function(productName){
    let quantity = this.products[productName];
        if (typeof quantity === 'undefined') {
            return 0;
        } else {
            return quantity;
        }

}
Warehouse.prototype.getMaxQuantity = function(){
    let max = 0;
        let maxProductName;

        for (const product in this.products) {

            if (this.products[product] > max) {
                max = this.products[product];
                maxProductName = product;
            }
        }
        return maxProductName;
}
Warehouse.prototype.changeProductQuantity = function(productName, quantity){
    if (quantity < 0) {
        console.log("Error");
        return;
    }

    let currentQuantity = this.products[productName] ? this.products[productName] : 0;
    let possibleNewTotalQuantity = this.getTotalQuantity() - currentQuantity + quantity;
    // console.log(this.products[productName])
    if (possibleNewTotalQuantity <= this.maxCapacity) {
        return this.products[productName] = quantity;
    } else {
        return "Error Max Capacity";
    }
}

const warehouseKyiv = new Warehouse(30, {
    phones: 2,
    headphones: 4,
    notebooks: 10,
    tablets: 7,
}
);
// console.log(warehouseKyiv);
const warehouseLviv = new Warehouse(50,{});
const warehouseKharkiv = new Warehouse(40,{});

warehouseLviv.changeProductQuantity('phones', 7);
warehouseLviv.changeProductQuantity('tablets', 10);

warehouseKharkiv.changeProductQuantity('phones', 10);
warehouseKharkiv.changeProductQuantity('notebooks', 6);
// console.log(warehouseLviv, warehouseKharkiv);

// console.log(warehouseLviv.getTotalQuantity());
// console.log(warehouseKyiv.getTotalQuantity());
// console.log(warehouseKharkiv.getTotalQuantity());

// console.log(warehouseKyiv.getProductQuantity("phones"));

// console.log(warehouseLviv.getMaxQuantity());

function getTotalQuantity(name, arr){
    let result = arr.reduce(function(sum, current) {
        // console.log(sum, current);
        return sum + current.getProductQuantity(name);
      }, 0);
    return result;
}

// console.log(getTotalQuantity('tablets', [warehouseKharkiv, warehouseKyiv, warehouseLviv]));

Warehouse.prototype.moveProduct = function (name, quantity, targetWarehouse){
    
    if(this.getProductQuantity(name) < quantity){
        console.log(   `There is no such quantity of ${name} in stock`);
    } 
    else
    {
        if((targetWarehouse.maxCapacity - (targetWarehouse.getTotalQuantity() + quantity)) < 0 ){
            console.log("Error Max Capacity");
        }
        else
        {
            let stockRest = this.getProductQuantity(name) - quantity;
            this.changeProductQuantity(name, stockRest)
            targetWarehouse.changeProductQuantity(name, quantity)
        }
    }
}

    

warehouseKyiv.moveProduct("notebooks", 4, warehouseLviv);
console.log(warehouseKyiv.getProductQuantity("notebooks"));
console.log(warehouseLviv.getProductQuantity("notebooks"));

