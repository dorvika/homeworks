import "./Header.scss";
import { NavLink, Link } from "react-router-dom";
import { useSelector } from "react-redux/es/exports";
import { productsInBasket } from "../../store/basket/reducer";
import { favorites } from "../../store/favorities/reducer";

export default function Header() {
  const countFavorites = useSelector(favorites).length;
  const basket = useSelector(productsInBasket);

  const totalProductsQTY = basket.reduce(
    (accumulator, currentValue) => accumulator + currentValue.qty,
    0
  );

  return (
    <header className="header">
      <nav>
        <ul className="header__nav">
          <li className="header__nav__item">
            <NavLink to="/">Головна</NavLink>
          </li>
          <li className="header__nav__item">
            <NavLink to="/favorites">Обране ({countFavorites})</NavLink>
          </li>
          <li className="header__nav__item">
            <NavLink to="/basket">Кошик ({totalProductsQTY})</NavLink>
          </li>
        </ul>
      </nav>
      <Link to="/">
        <img
          className="header__logo"
          src="./images/logo.jpg"
          alt="fashionista logo"
        />
      </Link>
    </header>
  );
}
