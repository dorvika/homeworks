import "./basketProductCard.scss";
import { useDispatch } from "react-redux";
import { addToBasket, deleteFromBasket } from "../../store/basket/actions";
import { openModal } from "../../store/modal/actions";

export default function BasketProductCard({ productObj }) {
  const dispatch = useDispatch();
  const { name, price, img, color, qty } = productObj;

  const onAdd = () => {
    dispatch(addToBasket(productObj));
  };

  return (
    <div className="cart__product">
      <img src={img} width={160} height={200} alt={name} />
      <div className="cart__product__info">
        <h3 className="cart__product__title">{name}</h3>
        <p>{price} грн</p>
        <p>Колір: {color}</p>
      </div>
      <div className="cart__product__counter">
        <button
          onClick={
            qty === 1
              ? () => {
                  dispatch(
                    openModal({
                      content: "Ви бажаєте видалити даний товар із кошика?",
                      action: "delete",
                      product: productObj,
                    })
                  );
                }
              : () => dispatch(deleteFromBasket(productObj.sku))
          }
        >
          -
        </button>
        <span>{qty}</span>
        <button onClick={onAdd}>+</button>
      </div>
      <div className="cart__product__sum"> {qty * price} грн</div>
      <button
        className="cart__product__delete"
        onClick={() => {
          dispatch(
            openModal({
              content: "Ви бажаєте видалити даний товар із кошика?",
              action: "delete",
              product: productObj,
              deleteTotallyFlag: true,
            })
          );
        }}
      >
        &times;
      </button>
    </div>
  );
}
