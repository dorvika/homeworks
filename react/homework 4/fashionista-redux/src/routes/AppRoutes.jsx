import { Route, Routes } from "react-router-dom";
import Basket from "../pages/Basket";
import Error from "../pages/Error";
import Favorites from "../pages/Favorites";
import Home from "../pages/Home";

export default function AppRoutes() {
  return (
    <main className="main">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/favorites" element={<Favorites />} />
        <Route path="/basket" element={<Basket />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </main>
  );
}
