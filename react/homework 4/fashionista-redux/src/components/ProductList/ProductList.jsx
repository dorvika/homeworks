import ProductCard from "../ProductCard";
import "./productList.scss";
import PropTypes from "prop-types";

export default function ProductList({ productsArray }) {
  const productCard = productsArray.map((product) => (
    <ProductCard key={product.sku} productObj={product} />
  ));
  return <div className="products">{productCard}</div>;
}

ProductList.propTypes = {
  productsArray: PropTypes.array.isRequired,
};
