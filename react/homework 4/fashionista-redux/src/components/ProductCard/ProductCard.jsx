import PropTypes from "prop-types";
import "./productCard.scss";
import { useDispatch, useSelector } from "react-redux";
import { toggleFavorite } from "../../store/favorities/actions";
import { openModal } from "../../store/modal/actions";
import { favorites } from "../../store/favorities/reducer";

export default function ProductCard({ productObj }) {
  const dispatch = useDispatch();
  const favoriteProducts = useSelector(favorites);
  const { name, price, img } = productObj;

  const favoriteIconClickHandler = () => {
    dispatch(toggleFavorite(productObj));
  };

  const showFavoriteIcon = (item) => {
    const isFavorite = favoriteProducts.find(
      (product) => product.sku === item.sku
    );
    if (isFavorite) return true;
    return false;
  };

  return (
    <div className="card">
      <img
        className="card__img"
        src={img}
        alt={name}
        width={260}
        height={320}
      />
      <img
        onClick={favoriteIconClickHandler}
        className="card__star__icon"
        src={
          showFavoriteIcon(productObj)
            ? "./images/icons/star.png"
            : "./images/icons/star_empty.png"
        }
        alt="star icon"
      />
      <h3 className="card__title">{name}</h3>
      <p className="card__price">{price} грн</p>
      <button
        className="card__btn"
        onClick={() =>
          dispatch(
            openModal({
              content: "Ви бажаєте додати даний товар до кошика?",
              action: "add",
              product: productObj,
            })
          )
        }
      >
        Додати до кошика
      </button>
    </div>
  );
}

ProductCard.propTypes = {
  productObj: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
    sku: PropTypes.number.isRequired,
  }),
};
