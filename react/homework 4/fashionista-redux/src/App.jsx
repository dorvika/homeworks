import "./App.scss";
import Header from "./components/Header";
import Footer from "./components/Footer";
import AppRoutes from "./routes/AppRoutes";
import Modal from "./components/Modal";

export default function App() {
  return (
    <div className="container">
      <Header />
      <AppRoutes />
      <Footer />
      <Modal />
    </div>
  );
}
