import { Route, Routes } from "react-router-dom";
import PropTypes from "prop-types";
import Basket from "../pages/Basket";
import Error from "../pages/Error";
import Favorites from "../pages/Favorites";
import Home from "../pages/Home";

export default function AppRoutes({
  products,
  productsInFavorite,
  productsInBasket,
  handleFavoriteProduct,
  addToBasket,
  deleteProductFromBasket,
}) {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <Home
            productsArray={products}
            handleFavoriteProduct={handleFavoriteProduct}
            addToBasket={addToBasket}
          />
        }
      />
      <Route
        path="/favorites"
        element={
          <Favorites
            favoriteProducts={productsInFavorite}
            handleFavoriteProduct={handleFavoriteProduct}
            addToBasket={addToBasket}
          />
        }
      />
      <Route
        path="/basket"
        element={
          <Basket
            productsInBasket={productsInBasket}
            deleteProductFromBasket={deleteProductFromBasket}
            addToBasket={addToBasket}
          />
        }
      />
      <Route path="*" element={<Error />} />
    </Routes>
  );
}

AppRoutes.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object),
  productsInFavorite: PropTypes.arrayOf(PropTypes.object),
  productsInBasket: PropTypes.arrayOf(PropTypes.object),
  handleFavoriteProduct: PropTypes.func,
  addToBasket: PropTypes.func,
  deleteProductFromBasket: PropTypes.func,
};
