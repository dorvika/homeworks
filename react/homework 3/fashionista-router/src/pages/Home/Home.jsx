import ProductList from "../../components/ProductList";
import PropTypes from "prop-types";

export default function Home({
  handleFavoriteProduct,
  addToBasket,
  productsArray,
}) {
  return (
    <section>
      <h2 className="section__title">Список товарів</h2>
      <ProductList
        productsArray={productsArray}
        handleFavoriteProduct={handleFavoriteProduct}
        addToBasket={addToBasket}
      />
    </section>
  );
}

Home.propTypes = {
  productsArray: PropTypes.arrayOf(PropTypes.object),
  handleFavoriteProduct: PropTypes.func,
  addToBasket: PropTypes.func,
};
