import ProductList from "../../components/ProductList";
import PropTypes from "prop-types";
import "./favorites.scss";

export default function Favorites({
  favoriteProducts,
  handleFavoriteProduct,
  addToBasket,
}) {
  return (
    <>
      {favoriteProducts.length === 0 ? (
        <p className="info">Ваш список обраних товарів пустий :(</p>
      ) : (
        <section>
          <h2 className="section__title">Обрані товари</h2>
          <ProductList
            handleFavoriteProduct={handleFavoriteProduct}
            productsArray={favoriteProducts}
            addToBasket={addToBasket}
          />
        </section>
      )}
    </>
  );
}

Favorites.propTypes = {
  favoriteProducts: PropTypes.arrayOf(PropTypes.object),
  handleFavoriteProduct: PropTypes.func,
  addToBasket: PropTypes.func,
};
