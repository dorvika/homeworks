import BasketProductCard from "../../components/BasketProductCard";
import PropTypes from "prop-types";
import "./basket.scss";

export default function Basket({
  productsInBasket,
  deleteProductFromBasket,
  addToBasket,
}) {
  const basketProduct = productsInBasket.map((product) => (
    <BasketProductCard
      key={product.sku}
      productObj={product}
      deleteProductFromBasket={deleteProductFromBasket}
      addToBasket={addToBasket}
    />
  ));

  const totalPrice = productsInBasket.reduce(
    (accumulator, currentValue) =>
      accumulator + currentValue.qty * currentValue.price,
    0
  );

  return (
    <>
      {productsInBasket.length === 0 ? (
        <p className="info">Ваш кошик пустий :(</p>
      ) : (
        <section className="cart">
          <h2 className="section__title">Список покупок</h2>
          {basketProduct}
          <footer className="cart__footer">
            <p className="cart__footer__text">Всього: {totalPrice} грн</p>
            <button
              className="cart__btn"
              onClick={() => console.log("Оформлюємо замовлення...")}
            >
              Оформити замовлення
            </button>
          </footer>
        </section>
      )}
    </>
  );
}

Basket.propTypes = {
  productsInBasket: PropTypes.arrayOf(PropTypes.object),
  deleteProductFromBasket: PropTypes.func,
  addToBasket: PropTypes.func,
};
