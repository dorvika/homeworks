import { useEffect, useState } from "react";
import "./App.scss";
import Header from "./components/Header";
import Footer from "./components/Footer";
import AppRoutes from "./routes/AppRoutes";

export default function App() {
  const [products, setProducts] = useState([]);
  const [productsInBasket, setProductsInBasket] = useState(
    JSON.parse(localStorage.getItem("Products in Basket")) || []
  );
  const [productsInFavorite, setproductsInFavorite] = useState(
    JSON.parse(localStorage.getItem("Products in Favorite")) || []
  );

  useEffect(() => {
    fetch("./api/products.json")
      .then((response) => response.json())
      .then((data) => setProducts(data));
  }, []);

  useEffect(() => {
    localStorage.setItem(
      "Products in Basket",
      JSON.stringify(productsInBasket)
    );
  }, [productsInBasket]);

  useEffect(() => {
    localStorage.setItem(
      "Products in Favorite",
      JSON.stringify(productsInFavorite)
    );
  }, [productsInFavorite]);

  const deleteProductFromBasket = (product, deleteTotallyFlag) => {
    const exist = searchProduct(productsInBasket, product);
    if (exist.qty === 1 || deleteTotallyFlag) {
      setProductsInBasket(filterProduct(productsInBasket, product));
    } else {
      setProductsInBasket(
        productsInBasket.map((item) =>
          item.sku === product.sku ? { ...exist, qty: exist.qty - 1 } : item
        )
      );
    }
  };

  const addToBasket = (product) => {
    const exist = searchProduct(productsInBasket, product);
    if (exist) {
      setProductsInBasket(
        productsInBasket.map((item) =>
          item.sku === product.sku ? { ...exist, qty: exist.qty + 1 } : item
        )
      );
    } else {
      setProductsInBasket([...productsInBasket, { ...product, qty: 1 }]);
    }
  };

  const handleFavoriteProduct = (product) => {
    const isFavorite = searchProduct(productsInFavorite, product);
    if (isFavorite) {
      const newArray = filterProduct(productsInFavorite, product);
      setproductsInFavorite(newArray);
    } else {
      setproductsInFavorite([...productsInFavorite, product]);
    }
  };

  const searchProduct = (place, product) => {
    const existingProduct = place.find((item) => item.sku === product.sku);
    return existingProduct;
  };

  const filterProduct = (place, product) => {
    return place.filter((item) => item.sku !== product.sku);
  };

  return (
    <div className="container">
      <Header
        countFavoriteProducts={productsInFavorite.length}
        countProductsInBasket={productsInBasket.length}
      />
      <main className="main">
        <AppRoutes
          products={products}
          productsInFavorite={productsInFavorite}
          productsInBasket={productsInBasket}
          handleFavoriteProduct={handleFavoriteProduct}
          addToBasket={addToBasket}
          deleteProductFromBasket={deleteProductFromBasket}
        />
      </main>
      <Footer />
    </div>
  );
}
