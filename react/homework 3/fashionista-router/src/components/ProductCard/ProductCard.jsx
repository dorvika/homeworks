import PropTypes from "prop-types";
import "./productCard.scss";
import Modal from "../Modal";
import { useState } from "react";

export default function ProductCard({
  handleFavoriteProduct,
  addToBasket,
  productObj,
}) {
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const modalHandler = () => {
    setModalIsOpen((prevValue) => !prevValue);
  };

  const { sku, name, price, img } = productObj;

  const favoriteIconClickHandler = () => {
    handleFavoriteProduct(productObj);
    if (JSON.parse(localStorage.getItem(`Favorite #${sku}`))) {
      localStorage.removeItem(`Favorite #${sku}`, sku);
    } else {
      localStorage.setItem(`Favorite #${sku}`, sku);
    }
  };

  return (
    <div className="card">
      <img
        className="card__img"
        src={img}
        alt={name}
        width={260}
        height={320}
      />
      <img
        onClick={favoriteIconClickHandler}
        className="card__star__icon"
        src={
          JSON.parse(localStorage.getItem(`Favorite #${sku}`))
            ? "./images/icons/star.png"
            : "./images/icons/star_empty.png"
        }
        alt="star icon"
      />
      <h3 className="card__title">{name}</h3>
      <p className="card__price">{price} грн</p>
      <button className="card__btn" onClick={modalHandler}>
        Додати до кошика
      </button>
      {modalIsOpen && (
        <Modal product={productObj} action={addToBasket} onClose={modalHandler}>
          Ви бажаєте додати даний товар до кошика?
        </Modal>
      )}
    </div>
  );
}

ProductCard.propTypes = {
  handleFavoriteProduct: PropTypes.func,
  addToBasket: PropTypes.func,
  productObj: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
    sku: PropTypes.number.isRequired,
  }),
};
