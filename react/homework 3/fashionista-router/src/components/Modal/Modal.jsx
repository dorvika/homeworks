import "./modal.scss";
import PropTypes from "prop-types";

export default function Modal({
  onClose,
  product,
  action,
  children,
  deleteTotallyFlag,
}) {
  const clickHandler = () => {
    action(product, deleteTotallyFlag);
    onClose();
  };

  return (
    <>
      <div className="modal">
        <div className="modal__header">
          <button className="modal__header__btn" onClick={onClose}>
            &times;
          </button>
        </div>
        <div className="modal__content">
          <p className="modal__content__text">{children}</p>
          <button className="modal__content__btn" onClick={clickHandler}>
            Ок
          </button>
          <button className="modal__content__btn" onClick={onClose}>
            Скасувати
          </button>
        </div>
      </div>
      <div className="backdrop" onClick={onClose} />
    </>
  );
}

Modal.propTypes = {
  product: PropTypes.object.isRequired,
  action: PropTypes.func,
  onClose: PropTypes.func.isRequired,
  deleteTotallyFlag: PropTypes.bool,
};
