import { useState } from "react";
import PropTypes from "prop-types";
import Modal from "../Modal";
import "./basketProductCard.scss";

export default function BasketProductCard({
  productObj,
  deleteProductFromBasket,
  addToBasket,
}) {
  const { name, price, img, color, qty } = productObj;

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const modalHandler = () => {
    setModalIsOpen((prevValue) => !prevValue);
  };

  return (
    <div className="cart__product">
      <img src={img} width={160} height={200} alt={name} />
      <div className="cart__product__info">
        <h3 className="cart__product__title">{name}</h3>
        <p>{price} грн</p>
        <p>Колір: {color}</p>
      </div>
      <div className="cart__product__counter">
        <button
          onClick={
            qty === 1 ? modalHandler : () => deleteProductFromBasket(productObj)
          }
        >
          -
        </button>
        <span>{qty}</span>
        <button onClick={() => addToBasket(productObj)}>+</button>
      </div>
      <div className="cart__product__sum"> {qty * price} грн</div>
      <button className="cart__product__delete" onClick={modalHandler}>
        &times;
      </button>
      {modalIsOpen && (
        <Modal
          product={productObj}
          action={deleteProductFromBasket}
          onClose={modalHandler}
          deleteTotallyFlag={true}
        >
          Ви бажаєте видалити товар з кошика?
        </Modal>
      )}
    </div>
  );
}

BasketProductCard.propTypes = {
  productObj: PropTypes.object,
  deleteProductFromBasket: PropTypes.func,
  addToBasket: PropTypes.func,
};
