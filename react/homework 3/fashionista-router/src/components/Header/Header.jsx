import "./Header.scss";
import PropTypes from "prop-types";
import { NavLink, Link } from "react-router-dom";

export default function Header({
  countFavoriteProducts,
  countProductsInBasket,
}) {
  return (
    <header className="header">
      <nav>
        <ul className="header__nav">
          <li className="header__nav__item">
            <NavLink to="/">Головна</NavLink>
          </li>
          <li className="header__nav__item">
            <NavLink to="/favorites">Обране ({countFavoriteProducts})</NavLink>
          </li>
          <li className="header__nav__item">
            <NavLink to="/basket">Кошик ({countProductsInBasket})</NavLink>
          </li>
        </ul>
      </nav>
      <Link to="/">
        <img
          className="header__logo"
          src="./images/logo.jpg"
          alt="fashionista logo"
        />
      </Link>
    </header>
  );
}

Header.propTypes = {
  countFavoriteProducts: PropTypes.number,
  countProductsInBasket: PropTypes.number,
};

Header.defaultProps = {
  countFavoriteProducts: 0,
  countProductsInBasket: 0,
};
