import ProductCard from "../ProductCard";
import "./productList.scss";
import PropTypes from "prop-types";

export default function ProductList({
  handleFavoriteProduct,
  addToBasket,
  productsArray,
}) {
  const productCard = productsArray.map((product) => (
    <ProductCard
      key={product.sku}
      productObj={product}
      handleFavoriteProduct={handleFavoriteProduct}
      addToBasket={addToBasket}
    />
  ));
  return <div className="products">{productCard}</div>;
}

ProductList.propTypes = {
  productsArray: PropTypes.array.isRequired,
  handleFavoriteProduct: PropTypes.func,
  addToBasket: PropTypes.func,
};
