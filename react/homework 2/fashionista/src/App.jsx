import "./App.scss";
import { Component } from "react";
import Header from "./components/Header/Header";
import ProductList from "./components/ProductList/ProductList";

const getProductsFromLS = (key) => {
  const lsProducts = localStorage.getItem(key);
  if (!lsProducts) return [];
  try {
    const value = JSON.parse(lsProducts);
    return value;
  } catch (e) {
    return [];
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);
    const lsCart = getProductsFromLS("Products in Cart");
    const lsFavorite = getProductsFromLS("Products in Favorite");
    this.state = {
      products: [],
      productsInCart: lsCart,
      productsInFavorite: lsFavorite,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.productsInCart !== this.state.productsInCart) {
      localStorage.setItem(
        "Products in Cart",
        JSON.stringify(this.state.productsInCart)
      );
    }

    if (prevState.productsInFavorite !== this.state.productsInFavorite) {
      localStorage.setItem(
        "Products in Favorite",
        JSON.stringify(this.state.productsInFavorite)
      );
    }
  }

  addToCart = (product) => {
    this.setState({
      productsInCart: [...this.state.productsInCart, product],
    });
  };

  handleFavoriteProduct = (product) => {
    const isFavorite = this.state.productsInFavorite.find(
      (isFavoriteProduct) => isFavoriteProduct.sku === product.sku
    );
    if (isFavorite) {
      const newArray = this.state.productsInFavorite.filter(
        (item) => item.sku !== product.sku
      );
      this.setState({ productsInFavorite: newArray });
    } else {
      this.setState({
        productsInFavorite: [...this.state.productsInFavorite, product],
      });
    }
  };

  componentDidMount() {
    fetch("./api/products.json")
      .then((response) => response.json())
      .then((data) =>
        this.setState({
          products: data,
        })
      );
  }

  render() {
    return (
      <div className="container">
        <Header
          countFavoriteProducts={this.state.productsInFavorite.length}
          countProductsInCart={this.state.productsInCart.length}
        />
        <ProductList
          productsArray={this.state.products}
          handleFavoriteProduct={this.handleFavoriteProduct}
          addToCart={this.addToCart}
        />
      </div>
    );
  }
}
