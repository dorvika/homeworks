import { Component } from "react";
import ProductCard from "../ProductCard/ProductCard";
import "./productList.scss";
import PropTypes from "prop-types";

export default class ProductList extends Component {
  render() {
    const { handleFavoriteProduct, addToCart } = this.props;
    const productCard = this.props.productsArray.map((product) => (
      <ProductCard
        key={product.sku}
        productObj={product}
        handleFavoriteProduct={handleFavoriteProduct}
        addToCart={addToCart}
      />
    ));
    return <main className="main">{productCard}</main>;
  }
}

ProductList.propTypes = {
  productsArray: PropTypes.array.isRequired,
  handleFavoriteProduct: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
};
