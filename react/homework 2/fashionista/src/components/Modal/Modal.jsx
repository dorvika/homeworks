import { Component } from "react";
import "./modal.scss";
import PropTypes from "prop-types";

class Modal extends Component {
  render() {
    const { onClose, addToCart, text, product } = this.props;
    return (
      <>
        <div className="modal">
          <div className="modal__header">
            <button className="modal__header__btn" onClick={onClose}>
              &times;
            </button>
          </div>
          <div className="modal__content">
            <p className="modal__content__text">{text}</p>
            <button
              className="modal__content__btn"
              onClick={() => {
                addToCart(product);
                onClose();
              }}
            >
              Ок
            </button>
            <button className="modal__content__btn" onClick={onClose}>
              Скасувати
            </button>
          </div>
        </div>
        <div className="backdrop" onClick={onClose} />
      </>
    );
  }
}

export default Modal;

Modal.propTypes = {
  product: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  text: PropTypes.string,
};

Modal.defaultProps = {
  text: "Текст модального вікна",
};
