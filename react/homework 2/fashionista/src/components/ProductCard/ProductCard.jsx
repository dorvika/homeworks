import { Component } from "react";
import PropTypes from "prop-types";
import "./productCard.scss";
import Modal from "../Modal/Modal";

export default class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
    };
  }

  showModal = () => {
    this.setState({
      modalIsOpen: true,
    });
  };

  closeModal = () => {
    this.setState({
      modalIsOpen: false,
    });
  };

  favoriteIconClickHandler = () => {
    const { sku } = this.props.productObj;
    this.props.handleFavoriteProduct(this.props.productObj);
    if (JSON.parse(localStorage.getItem(`Favorite #${sku}`))) {
      localStorage.removeItem(`Favorite #${sku}`, sku);
    } else {
      localStorage.setItem(`Favorite #${sku}`, sku);
    }
  };

  render() {
    const { name, price, img, sku } = this.props.productObj;
    return (
      <div className="card">
        <img className="card__img" src={img} alt={name} />
        <img
          onClick={this.favoriteIconClickHandler}
          className="card__star__icon"
          src={
            JSON.parse(localStorage.getItem(`Favorite #${sku}`))
              ? "./images/icons/star.png"
              : "./images/icons/star_empty.png"
          }
          alt="star icon"
        />
        <h3 className="card__title">{name}</h3>
        <p className="card__price">{price} грн</p>
        <button className="card__btn" onClick={this.showModal}>
          Додати до кошика
        </button>
        {this.state.modalIsOpen && (
          <Modal
            product={this.props.productObj}
            addToCart={this.props.addToCart}
            onClose={this.closeModal}
            text="Ви бажаєте додати даний товар до кошика?"
          />
        )}
      </div>
    );
  }
}

ProductCard.propTypes = {
  handleFavoriteProduct: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  productObj: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
    sku: PropTypes.number.isRequired,
  }),
};
