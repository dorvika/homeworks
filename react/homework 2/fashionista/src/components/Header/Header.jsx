import { Component } from "react";
import "./Header.scss";
import PropTypes from "prop-types";

export default class Header extends Component {
  render() {
    const { countFavoriteProducts, countProductsInCart } = this.props;
    return (
      <header className="header">
        <div className="header__icon__container">
          <img
            src="./images/icons/star_empty.png"
            alt="empty star icon"
            className="star__icon"
          />
          <p className="icon__text">{countFavoriteProducts}</p>
          <img
            src="./images/icons/basket_empty.png"
            alt="basket icon"
            className="basket__icon"
          />
          <p className="icon__text">{countProductsInCart}</p>
        </div>
        <a href="index.html">
          <img
            className="header__logo"
            src="./images/logo.jpg"
            alt="fashionista logo"
          />
        </a>
      </header>
    );
  }
}

Header.propTypes = {
  countFavoriteProducts: PropTypes.number,
  countProductsInCart: PropTypes.number,
};

Header.defaultProps = {
  countFavoriteProducts: 0,
  countProductsInCart: 0,
};
