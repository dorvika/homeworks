import "./App.scss";
import { Component } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      modalInfo: {},
    };
  }

  generateModal = (modalInfoObject) => {
    this.setState({
      modalIsOpen: true,
      modalInfo: modalInfoObject,
    });
  };

  closeModal = () => {
    this.setState({
      modalIsOpen: false,
      modalInfo: {},
    });
  };

  render() {
    return (
      <div className="container">
        <Button
          dataModalId="modalID1"
          text="Open first modal"
          backgroundColor="red"
          onClick={this.generateModal}
        />
        <Button
          dataModalId="modalID2"
          text="Open second modal"
          backgroundColor="blue"
          onClick={this.generateModal}
        />
        {this.state.modalIsOpen && (
          <Modal onClose={this.closeModal} modalState={this.state.modalInfo} />
        )}
      </div>
    );
  }
}

export default App;
