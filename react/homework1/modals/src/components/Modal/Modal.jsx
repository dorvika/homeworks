import { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  closeHandler = () => {
    this.props.onClose();
  };

  render() {
    const { header, closeButton, text, actions } = this.props.modalState;
    return (
      <>
        <div className="modal">
          <div className="modal__header">
            <h2 className="modal__header__title">{header}</h2>
            {closeButton && (
              <button
                className="modal__header__btn"
                onClick={this.closeHandler}
              >
                &times;
              </button>
            )}
          </div>
          <div className="modal__content">
            <p className="modal__content__text">{text}</p>
            {actions}
          </div>
        </div>
        <div className="backdrop" onClick={this.closeHandler} />
      </>
    );
  }
}

export default Modal;
