import { Component } from "react";
import modalWindowDeclarations from "../../modalData";
import "./button.scss";

class Button extends Component {
  clickHandler = (e) => {
    const modalID = e.target.dataset.modalId;
    const modalDeclaration = modalWindowDeclarations.find(
      (item) => item.id === modalID
    );
    this.props.onClick(modalDeclaration);
  };

  render() {
    const { text, backgroundColor, dataModalId } = this.props;
    return (
      <button
        data-modal-id={dataModalId}
        className={`btn ${backgroundColor}`}
        onClick={this.clickHandler}
      >
        {text}
      </button>
    );
  }
}

export default Button;
