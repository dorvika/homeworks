const modalWindowDeclarations = [
  {
    id: "modalID1",
    header: "Do you want to delete this file?",
    closeButton: true,
    text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
    actions: (
      <>
        <button className="modal__content__btn">Ok</button>
        <button className="modal__content__btn">Cancel</button>
      </>
    ),
  },
  {
    id: "modalID2",
    header: "Do you want to move this file to another folder?",
    closeButton: true,
    text: "Once you move this file, it won't be possible to undo this action. Are you sure you want to move it?",
    actions: (
      <>
        <button className="modal__content__btn">Confirm</button>
        <button className="modal__content__btn">Cancel</button>
      </>
    ),
  },
];

export default modalWindowDeclarations;
