"use strict"

// Опишите своими словами, что такое метод обьекта

/* Метод об*єкту - це свойство об*єкту, значенням якого виступає функція. Такі функції працюють із свойствами об*єкту всередині об*єкту, для того щоб не засмічувати глобальну область видимості. Тобто в об*єкті ми беремо дані, над якими потім у цьому ж об*єкті метод здійснює ті чи інші операції та повертає результат. */


const createNewUser = function (){
    const firstName = prompt("Enter your name");
    const lastName = prompt("Enter your last name");
    const newUser = {
        firstName,
        lastName,
        getLogin(paramName,paramLastName){
            paramName = this.firstName.slice(0, 1).toLowerCase();
            paramLastName = this.lastName.toLowerCase();
            return paramName + paramLastName;
        },
        setFirstName(newFName){
            if (newFName){
            Object.defineProperty(newUser,"firstName",{
            value: newFName
            })
            }
            },
        setLastName(newLName){
            if (newLName){
            Object.defineProperty(newUser,"lastName",{
            value: newLName,
            })
            }
        },
    }
    Object.defineProperty(newUser,"firstName",{
        value: newUser.firstName,
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser,"lastName",{
            value: newUser.lastName,
            writable: false,
            configurable: true,
    });

    // Перевірка можливості зміни значень свойств напряму та через функції-сетери:

    // newUser.firstName = "Ivan"; 
    // newUser.lastName = "Artemenko";
    // newUser.setFirstName("Ivan");
    // newUser.setLastName("Artemenko");

   // console.log(newUser.getLogin());
    return(newUser);
}

const user = createNewUser();
console.log(user.getLogin());
