"use strict"

const parent = document.querySelector(".password-form");

function showHide (input, icon){
    if(input.getAttribute("type") === "password"){
        input.setAttribute("type", "text");
        icon.classList.toggle("fa-eye-slash");
    } else {
        input.setAttribute("type", "password");
        icon.classList.toggle("fa-eye-slash");
    }
}

parent.addEventListener('click', event => {
    if(event.target.matches("i")){
        const iconElm = event.target;
        const inputElm = iconElm.previousElementSibling;
        showHide(inputElm, iconElm);
    }
})

const button = document.querySelector(".btn")
const input1 = document.querySelector(".input1")
const input2 = document.querySelector(".input2")

button.addEventListener("click", validatePassword);

const error = document.createElement ("p");
error.classList.add("error")

function validatePassword (event){
    event.preventDefault()
    if (input1.value !== input2.value || !input1.value.trim() || !input2.value.trim()) {
        error.textContent = "Нужно ввести одинаковые значения"
        button.before(error)
    } else {
        error.textContent = ''
        alert ("You are welcome");
    }
}