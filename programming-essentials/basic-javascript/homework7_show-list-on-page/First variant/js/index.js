"use strict"

// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

/*DOM - це об'єктна модель документу, яка представляє сторінку у вигляді об'єктів, які можна змінювати, додавати, видаляти і т.д.
DOM дозволяє взаємодіяти з кожним елементом на сторінці, прописувати для нього окремий код через інші мови програмування (наприклад, JavaScript) і додавати динамічності та інтерактивності сторінкам.*/


const listToShow = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const body = document.querySelector("body") // на випадок якщо користувач не вкаже другий аргумент функції
const container = document.querySelector(".container")

function showList (array, parentEl = body){

const list = document.createElement("ul");
parentEl.prepend(list)

const htmlList = array.map((e) => {
    const listItem = document.createElement("li");
    listItem.textContent = `${e}`;
    return listItem
})

const htmlBlock = document.createDocumentFragment();

htmlList.forEach((e) => {
    htmlBlock.append(e);
})

list.append(htmlBlock);
}

showList (listToShow, container)