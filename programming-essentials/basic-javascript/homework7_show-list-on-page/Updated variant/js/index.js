"use strict"

const pageList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function listToShow (array, parentEl = document.body){
const newList = array.map((el) => {
    el = `<li>${el}</li>`
    return el
})
const itemsToShow = newList.join("")
const list = document.createElement("ul")
parentEl.prepend(list)
list.insertAdjacentHTML('afterbegin', itemsToShow)
}

listToShow(pageList)


