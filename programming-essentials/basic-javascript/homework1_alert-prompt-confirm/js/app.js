// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

/* var
1. функціональна область видимості - змінна оголошена за допомогою var не покине межі функції і звернення до неї 
поза межею даної функції призведе до помилки. Але var ігнорує всі інші межі, наприклад, умовного оператора if; у цьому випадку ми можемо
звертатися до змінної і в межах умовного оператора if, і в межах глобальної видимості, що може призвести до непотрібної заміни значень змінних та
майбутніх помилок. Адже ми очікували одну змінну в межах умовної конструкції, і зовсім іншу - в межах глобальної видимості, а отримали спільну
змінну на обидві зони видимості.
2. змінна з однією назвою може бути оголошена повторно в межах однієї і тієї ж самої зони видимості, і це НЕ призведе до помилки в console.
3. ми можемо звертатися до змінної ДО моменту її оголошення (undefined).

let
1. блочна область видимості - змінна оголошення за допомогою let не покине межі {}, ми можемо бути впевненими, що змінна в межах {} ніяким
чином не потрапить до інших зон видимості та не призведе до помилок.
2. змінна з однією і тією ж назвою НЕ може бути оголошена повторно в межах однієї і тієї ж зони видимості.
3. ReferenceError коли ми звертаємося до змінної до моменту її оголошення.

const
1. все те, що притаманне let.
2. значення змінної НЕ може бути перезаписане.
*/

// Почему объявлять переменную через var считается плохим тоном?

/* По-перше, тому що оголошення змінних через var була прописана у попередній версії ES, яка вже є застарілою.
Сьогодні діє новий стандарт (ES6), який рекомендує оголошувати змінні виключно через let або const.
По-друге, через функціональну, а НЕ блочну зону видимості var, що може слугувати джерелом для подальших помилок у коді, які буде складно знайти.
По-третє, через можливість повторно оголошувати змінну з тією ж назвою в межах однієї зони видимості (дублювання змінних).*/

// 1 варіант

// let name;
// while (!(name = prompt("Enter your name:"))){
//     alert("This field can*t be empty! Write your name!");
// }

// let age = prompt("Enter your age:");

// if ((age && !isNaN(age) && typeof +age === "number") === false) {
//         age = prompt(`Only valid values are acceptable! Rewrite your age: ${age}`)
//          }
//     if (age < 18){
//         alert("You are not allowed to visit this website");
//      } else if (18 <= age && age <= 22)
//              { let answer = confirm("Are you sure you want to continue?");
//                 if (answer){
//                     alert (`Welcome, ${name}`);
//                 }
//                 else {
//                     alert ("You are not allowed to visit this website");
//                 }
//             }
//         else { alert (`Welcome, ${name}`);}

// 2 варіант

let name;
while (!(name = prompt("Enter your name:"))){
    alert("This field can*t be empty! Rewrite your name!");
}

let age = prompt("Enter your age:");

while (!(age && !isNaN(age) && typeof +age === "number")) {
        age = prompt(`Only valid values are acceptable! Rewrite your age: ${age}`)
         }
if (age < 18){
        alert("You are not allowed to visit this website");
     } else if (18 <= age && age <= 22)
             { let answer = confirm("Are you sure you want to continue?");
                if (answer){
                    alert (`Welcome, ${name}`);
                }
                else {
                    alert ("You are not allowed to visit this website");
                }
            }
        else { alert (`Welcome, ${name}`);}

         
    
    
    
    
    
  
