"use strict"

// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

/* Екранирование символів - це додавання "\" перед символами, які можуть бути розпізнані та виконані як частина коду, а НЕ як частина строки. Наприклад, для того щоб строка 'It's me' відобразилася без помилки, перед ' потрібно поставити \ ('It\'s me')*/


const createNewUser = function (){
    const firstName = prompt("Enter your name");
    const lastName = prompt("Enter your last name");
    let birthday = prompt("Enter your birth year", "dd.mm.yyyy")

    const newUser = {
        firstName,
        lastName,
        birthday,
        getAge(date){
            let d = this.birthday.split('.');
            if( typeof d[2] !== "undefined"){
            date = d[2]+'.'+d[1]+'.'+d[0];
            let age = ((new Date().getTime() - new Date(date)) / (24 * 3600 * 365.25 * 1000)) | 0;
            return age
            }
        },
        getPassword(paramName,paramLastName, year){
            paramName = this.firstName.slice(0, 1).toUpperCase();
            paramLastName = this.lastName.toLowerCase();
            let d = this.birthday.split('.');
            year = d[2];
            return paramName +  paramLastName + year;
        },
        getLogin(paramName,paramLastName){
            paramName = this.firstName.slice(0, 1).toLowerCase();
            paramLastName = this.lastName.toLowerCase();
            return paramName + paramLastName;
        },
        setFirstName(newFName){
            if (newFName){
            Object.defineProperty(newUser,"firstName",{
            value: newFName
            })
            }
        },
        setLastName(newLName){
            if (newLName){
            Object.defineProperty(newUser,"lastName",{
            value: newLName,
            })
            }
        },
    }
    Object.defineProperty(newUser,"firstName",{
        value: newUser.firstName,
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser,"lastName",{
            value: newUser.lastName,
            writable: false,
            configurable: true,
    });

   return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

