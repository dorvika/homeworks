"use strict"

// Опишите своими словами как работает цикл forEach.

/* Цикл forEach перебирає значення елементів масиву та змінює їх відповідно до коду, прописаного у callback функції.
Наприклад, у нас є певний масив, за допомогою методу forEach та callback функції ми можемо дістатися до кожного значення елементів масиву окремо, змінити їх так, як ми пропишемо у callback функції, і повернути назад "преобразований" масив.
ВАЖЛИВО! Метод(цикл) forEach повертає преобразований СТАРИЙ масив, а НЕ новий масив. */


const array = ['hello', " ", 23, null, undefined, false, {name:"Nick", age: 28}];

// forEach

function filterBy(array, dataType){
    const newArray = [];
    array.forEach(function(item){
        if (item === null && dataType === "null"){
            const myIndex = array.indexOf(null);
            const removed = newArray.splice(myIndex, 1);
        } else if (typeof item !== dataType || item === null && dataType === "object"){
           newArray.push(item);
        }
        
    })
    return newArray;
}
console.log(filterBy (array, "null"));

// filter

function filterBy(array, dataType){
    const newArray = [];
    array.filter(function(item){
        if (item === null && dataType === "null"){
            const myIndex = array.indexOf(null);
            const removed = newArray.splice(myIndex, 1);
        } else if (typeof item !== dataType || item === null && dataType === "object"){
           newArray.push(item);
        }
        
    })
    return newArray;
}
console.log(filterBy (array, "object"));
