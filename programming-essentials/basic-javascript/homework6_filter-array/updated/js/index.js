"use strict"

const array = ['hello', " ", "", 23, null, undefined, false, {name:"Nick", age: 28}, [1, 2, 3, 4]];

function filterBy(array, dataType){
    const newArray = array.filter((item) => {
        if (item === null){
            if (dataType === "null"){
                return false
            } else
            {
                return true
            }
        } 
        return typeof item !== dataType
    })
    return newArray
}

console.log(filterBy(array, "null"));
