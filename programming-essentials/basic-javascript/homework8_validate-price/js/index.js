"use strict"

// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

/*Обробник події - це функція, яка спрацює лише після того як з елементом, на який призначений обробник події, сталася певна подія (клік, відведення фокусу, скрол, натиснення на кнопку і т.д.) */

let input = document.createElement("input")
input.setAttribute("placeholder", "Price")
input.classList.add("page_input", "input")
document.body.prepend(input)
const errorMessage = document.createElement("p");

input.addEventListener('blur', validatePrice);

function validatePrice(){
        if (this.value == false || isNaN(this.value) || typeof +this.value !== "number" || this.value < 0)
            {
            errorMessage.innerHTML = "Please enter correct price";
            input.after(errorMessage);
            input.classList.remove("active_input", "input");
            input.classList.add("error_input");
            }
        else 
            {
            errorMessage.innerHTML = "";
            let span = document.createElement('span');
            span.innerHTML = "Текущая цена: " + this.value + " UAH";
            input.before(span);
            input.classList.remove("error_input");
            input.classList.add("active_input", "input");
            let closeBtn = document.createElement("span");
            closeBtn.classList.add("close_btn");
            closeBtn.innerHTML = "&times;";
            span.after(closeBtn);
            closeBtn.addEventListener("click", remove)
                function remove (){
                span.remove();
                closeBtn.remove();
                input.value = "";
                }
            }
}


