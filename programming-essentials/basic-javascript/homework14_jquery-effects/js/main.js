"use strict"

$(document).ready(function(){
    $("#page_links").on("click", "a", function(event){
        event.preventDefault();
        let id = $(this).attr("href");
        let top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    })

    const btn = $('#button');
    $(window).scroll(function() {     
    if ($(window).scrollTop() > $(window).height()) {
       btn.addClass('show');
     } else {
       btn.removeClass('show');
     }
   });
   btn.on('click', function(e) {
     e.preventDefault();
     $('html, body').animate({scrollTop:0}, '300');
   });

   $("#hide_btn").on("click", function(){
    $("#posts_container").toggle("slow")
   })

})