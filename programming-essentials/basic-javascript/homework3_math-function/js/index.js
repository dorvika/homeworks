"use strict"

// Описать своими словами для чего вообще нужны функции в программировании.

/* Функція - це шматок коду, який можна викликати та виконати у будь-якому місці коду БЕЗ його дублювання.
Викличаючи функцію, в неї можна передавати значення нових аргументів та отримувати новий результат.
Тобто функція допомагає не лише НЕ дублювати код, а й будь-який момент адаптувати результат, відхтовшуючись від значень
нових аргументів.
*/
// Описать своими словами, зачем в функцию передавать аргумент.

/* Аргументи - це конкретні значення, операції над якими будуть здійснені в ході виконання функції.
Тобто це дані, з якими буде працювати функція та повертати результат, відхтовхуючись від значень переданих аргументів*/

let firstNumber = prompt ("Введите 1 число!");
let secondNumber = prompt ("Введите 2 число!");
let operation = prompt ("Выберите действие: +, -, *, /");

while (
    !(firstNumber && !isNaN(firstNumber) && typeof +firstNumber === "number") ||
    !(secondNumber && !isNaN(secondNumber) && typeof +secondNumber === "number") ) 
{
    alert ("⛔️ Ошибка! Одно из введённых чисел не является числом.");
    firstNumber = prompt (`Введите корректное 1 число: ${firstNumber}`);
    secondNumber = prompt (`Введите корректное 2 число: ${secondNumber}`);
}

const mathFunction = function (num1, num2, action){
        if (action === "+"){
            return +num1 + +num2;
        } else if (action === "-"){
            return num1 - num2;
        } else if (action === "*"){
            return num1 * num2;
        } else if (action === "/"){
            return num1 / num2;
        } 
    }

console.log(mathFunction (firstNumber, secondNumber, operation));