"use strict"

// Опишите своими словами разницу между функциями setTimeout() и setInterval().

/* setTimeout() дозволяє викликати функцію 1 раз через певний проміжок часу. setInterval() дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.  */

// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

/* Якщо у функцію setTimeout() передати нульову затримку, така функція виконається відразу після завершення виконання поточного коду.
Тобто вона не спрацює миттєво, але вона виконається якомога швидше після виконання поточного коду.*/

// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

/* По-перше, якщо ми не викличемо clearInterval(), тоді функція setInterval() буде продовжувати виконуватися, коли нам цього вже не потрібно. По-друге, для того щоб не перевантажувати браузер та процесор.  */

const sliderWrap = document.querySelector('.slider-wrap');

const stopBtn = document.createElement("button");
stopBtn.textContent = "Прекратить";
stopBtn.classList.add("btn");

const resumeBtn = document.createElement("button");
resumeBtn.textContent = "Возобновить показ";
resumeBtn.classList.add("btn");

let btnCreation = setTimeout(creation, 3000);
function creation (){
    sliderWrap.append(stopBtn);
    sliderWrap.append(resumeBtn);
}

let images = document.querySelectorAll(".image-to-show");
let current = 0;

let slider = setTimeout(sliderFn, 3000)

function sliderFn(){
    images.forEach((el) => {
        el.classList.add("opacity0") 
    })
    images[current].classList.remove("opacity0");
    
    if(current+1 == images.length){
        current = 0
    } else {
        current++;
    }
    slider = setTimeout(sliderFn, 3000)
}

stopBtn.addEventListener('click', () => {
    clearTimeout(slider)
});
resumeBtn.addEventListener('click', () => {
    slider = setTimeout(sliderFn, 3000)
});

