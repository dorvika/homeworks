"use strict"

// Почему для работы с input не рекомендуется использовать события клавиатуры?

/* Тому що події клавіатури призначені для роботи з клавіатурою, тобто вони передбачають натиснення на клавіші. Якщо для роботи з input використовувати події клавіатури, вони можуть не запуститися, коли користувач використає альтернативні методи введення інформації (вставити скопійовваний текст, голосове введення інформації і т.д.). */

const btns = document.querySelectorAll(".btn");
document.addEventListener('keypress', hightlight)

function hightlight(event){
    btns.forEach (item => {
     if (event.code === item.getAttribute("data-tab")){
         item.classList.add("active")
     } else {
         item.classList.remove("active")
     }
 })
}