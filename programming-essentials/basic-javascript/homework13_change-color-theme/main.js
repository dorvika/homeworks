"use strict"

const container = document.querySelector(".container");

const btn = document.createElement("button");
btn.textContent = "Сменить тему";
btn.classList.add("change_theme_btn");
container.prepend(btn);

const theme = document.createElement("link");
theme.setAttribute("rel", "stylesheet")
theme.setAttribute("href", "./css/style2.css")

btn.addEventListener('click', changeColor)

let isPressBtn = false;

function changeColor(){
    if (!isPressBtn){
        document.querySelector("head").append(theme)
        isPressBtn = true;
        localStorage.setItem("theme", "blue");
    } else {
        theme.remove();
        isPressBtn = false;
        localStorage.removeItem("theme")
    }
}

if (localStorage.getItem("theme") !== null) {
    document.querySelector("head").append(theme)
} else {
    theme.remove();
}