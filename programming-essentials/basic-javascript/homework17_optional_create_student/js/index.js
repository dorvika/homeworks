"use strict"

const student = {};

const studentName = prompt("Ваше имя");
const studentLastName = prompt("Ваша фамилия");

student.name = studentName;
student.lastName = studentLastName;
student.tabel = {};

while(true) {
  const courseName = prompt("Название предмета");

  if(!courseName) {
      break;
  }

  const courseGrade = +prompt("Оценка по предмету");
  student.tabel[courseName] = courseGrade;
}

let badGrades = false;

for (let key in student.tabel) {
  if (student.tabel[key] < 4) {
    badGrades = true;
    break;
  }
}

if (!badGrades) {
  console.log("Студент переведен на следующий курс");
  let averageGrade = 0;
  let courseCont = 0;

  for (let key in student.tabel) {
    courseCont++;
    averageGrade +=student.tabel[key];
  }

  averageGrade = averageGrade/courseCont;

  if (averageGrade > 7) {
    console.log("Студенту назначена стипендия");
  }
}